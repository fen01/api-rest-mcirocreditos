<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Persona;
use App\Http\Controllers\personaController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::post('/insertar','personaController@insertar');
Route::get('/getAll',[personaController::class,'getAll']);
Route::post('/insertar',[personaController::class,'insertar']);
Route::post('/modificar',[personaController::class,'modificar']);
Route::post('/eliminar',[personaController::class,'eliminar']);
Route::get('/get_cuotas_persona',[personaController::class,'get_cuotas_persona']);



Route::get('/personas/{person}', function (Persona $person) {
    return response()->json($person);
});
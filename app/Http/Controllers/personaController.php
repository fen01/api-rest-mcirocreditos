<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Persona;

use DB;

class personaController extends Controller
{
  
    public function getAll(){
            $personas = Persona::all();
            return response()->json($personas);
    }
    
    public function insertar(Request $request){
        
        $persona = new Persona();
        
        $persona->first_name=$request->first_name;
        $persona->last_name=$request->last_name;
        $persona->address=$request->address;
        $persona->n_document=$request->n_document;

        $persona->save();
        

        return response()->json('success');
    }
    public function modificar (Request $request){
        
        $persona = Persona::find($request->id);
        
        $persona->first_name=$request->first_name;
        $persona->last_name=$request->last_name;
        $persona->address=$request->address;
        $persona->n_document=$request->n_document;

        $persona->save();
        

        return response()->json('update_success');
    }
    
    public function eliminar(Request $request){
        $persona = Persona::find($request->id);
        if($persona==null)
            return response()->json('person_not_fount');
        
        $persona->delete();

        return response()->json('person_delete');
}
public function get_cuotas_persona(Request $request){
  $cuotas=DB::table('personas as p')
            ->join('cuotas as c', 'p.id', '=', 'c.persona')            
            ->get();

    return response()->json($cuotas);
}

}
